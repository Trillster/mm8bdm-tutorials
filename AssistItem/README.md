# Elec'n Hover

A basic file which implements an assist item to demonstrate the full process for creating one while leveraging MM8BDM V6B's features.

## CBINS

This project is built using [Can't Believe It's Not SLADE](https://gitlab.com/Trillster/cbins), an open-source command line interface for compiling directory structures and their ACS into PK3 format.

For basic usage, navigate to this directory in terminal (or double click on the `cmdhere.bat`) and use the following command to build a playable PK3 in the `dist` directory.
```
cbins build
```