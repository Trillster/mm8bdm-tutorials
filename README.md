# MM8BDM Tutorials

This repository hosts all of the main example files which can be found in Mega Man 8-Bit Deathmatch modding documentation.

This repository is primarily for organizational purposes rather than educational. If intending to learn how to mod for MM8BDM, you should instead refer to the [wiki's modding documentation section](https://mm8bdm.notion.site/Mega-Man-8-Bit-Deathmatch-Wiki-8ef926aae3864135bce35cd050af38cc).