# Hazards and Oil

A tutorial file which provides example hazards and basic oil pits, both leveraging MM8BDM V6B features to award credit to the activator of the hazard.

## CBINS

This project is built using [Can't Believe It's Not SLADE](https://gitlab.com/Trillster/cbins), an open-source command line interface for compiling directory structures and their ACS into PK3 format.

For basic usage, navigate to this directory in terminal (or double click on the `cmdhere.bat`) and use the following command to build a playable PK3 in the `dist` directory.
```
cbins build
```