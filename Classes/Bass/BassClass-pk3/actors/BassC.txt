actor BassC : ClassBase
{
	player.scoreicon "103ST00"
	player.displayname "Bass"
	player.soundclass "bassc"

	player.forwardmove 1.01, 1.01
	player.sidemove 0.99, 0.99

	player.jumpz 10
	gravity 0.8
	player.startitem "AirJumpLimit", 1
	player.startitem "AirJumpZ", 12
	player.startitem "AirJumpZDecimal", 5000

	Health 75
	Player.MaxHealth 75
	
	// Required always
	player.startitem "BaseFlagPack", 1
	
	// Gives LMS and instagib weapons
	player.startitem "ModeWeps", 1
	
	// Custom instagib modifier
	player.startitem "BassInstagib", 1
	
	// LMS modifiers
	player.startitem "NeverReceiveLMSRanged", 1
	player.startitem "AlwaysReceiveLMSAoE", 1
	
	// Disables Buster Upgrades (too much ego)
	player.startitem "NoBusterUpgrades", 1
	
	// Actual class inventory
	player.startitem "BassBuster"
	player.startitem "BusterAmmo", 3
	States
	{
		Spawn:
			BASS A 0
			BASS B 1
			BASS A 1
			Goto Spawn+2
		See:
			BASS BCDE 5
			Goto Spawn
			
		Missile:
			BASS F 5
			BASS G 4
			goto Spawn+2
			
		ClassPain:
			BASS H 0
			goto MegamanPain
		ClassDeath:
			BASS H 0
			goto MegamanDeath
	}
}

actor BassInstagib : CustomInventory
{
	states
	{
		Pickup:
			TNT1 A 0 ACS_NamedExecuteAlways("bass_handleinstagib")
			stop
	}
}