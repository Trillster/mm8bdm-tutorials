# Advanced Spark Scatter

An extension to the basic Spark Scatter which implements more features to get people closer to the level of content which is more commonly produced, but still using the tools provided by MM8BDM V6B.

## CBINS

This project is built using [Can't Believe It's Not SLADE](https://gitlab.com/Trillster/cbins), an open-source command line interface for compiling directory structures and their ACS into PK3 format.

For basic usage, navigate to this directory in terminal (or double click on the `cmdhere.bat`) and use the following command to build a playable PK3 in the `dist` directory.
```
cbins build
```